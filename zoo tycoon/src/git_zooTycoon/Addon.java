package git_zooTycoon;

public class Addon {

	private String name;
	private int idNummer;
	private String bezeichnung;
	private double preis;
	private int bestandIst;
	private int bestandMax;
	// Ende Attribute

	public Addon(String name, int idNummer, String bezeichnung, double preis, int bestandIst, int bestandMax) {
		this.name = name;
		this.idNummer = idNummer; 
		this.bezeichnung = bezeichnung;
		this.preis = preis;
		this.bestandIst = bestandIst;
		this.bestandMax = bestandMax;
		
		}

	// Anfang Methoden
	public String getName() {
		return name;
	}

	public void setName(String nameNeu) {
		name = nameNeu;
	}

	public int getIdNummer() {
		return idNummer;
	}

	public void setIdNummer(int idNummerNeu) {
		idNummer = idNummerNeu;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnungNeu) {
		bezeichnung = bezeichnungNeu;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preisNeu) {
		preis = preisNeu;
	}

	public int getBestandIst() {
		return bestandIst;
	}

	public void setBestandIst(int bestandIstNeu) {
		bestandIst = bestandIstNeu;
	}

	public int getBestandMax() {
		return bestandMax;
	}

	public void setBestandMax(int bestandMaxNeu) {
		bestandMax = bestandMaxNeu;
	}

	// Ende Methoden
} // end of Addon
